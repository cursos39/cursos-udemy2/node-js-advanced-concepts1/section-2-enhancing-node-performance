# Benchmarking

````
ab -c 50 -n 500 localhost:3000/fast
````

Result:

```
➜  section-2-enhancing-node-performance git:(master) ab -c 50 -n 500 localhost:3000/fast
This is ApacheBench, Version 2.3 <$Revision: 1879490 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)
Completed 100 requests
Completed 200 requests
Completed 300 requests
Completed 400 requests
Completed 500 requests
Finished 500 requests


Server Software:        
Server Hostname:        localhost
Server Port:            3000

Document Path:          /fast
Document Length:        14 bytes

Concurrency Level:      50
Time taken for tests:   0.209 seconds
Complete requests:      500
Failed requests:        0
Total transferred:      106500 bytes
HTML transferred:       7000 bytes
Requests per second:    2387.11 [#/sec] (mean)
Time per request:       20.946 [ms] (mean)
Time per request:       0.419 [ms] (mean, across all concurrent requests)
Transfer rate:          496.54 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.4      0       3
Processing:     2   19   3.3     20      27
Waiting:        1   19   3.3     19      26
Total:          4   19   3.1     20      27

Percentage of the requests served within a certain time (ms)
  50%     20
  66%     21
  75%     21
  80%     22
  90%     23
  95%     24
  98%     25
  99%     26
 100%     27 (longest request)
 ```

 # Clustering

 Install pm2 in global.

 ```shell
 npm install -g pm2
 ```